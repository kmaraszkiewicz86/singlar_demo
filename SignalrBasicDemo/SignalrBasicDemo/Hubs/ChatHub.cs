﻿using Microsoft.AspNetCore.SignalR;

namespace SignalrBasicDemo.Hubs
{
    public class ChatHub : Hub
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ChatHub(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public Task SendMessage(string message)
        {
            return Clients.All.SendAsync("SendChatMessage", $"User_{_httpContextAccessor.HttpContext!.TraceIdentifier}", message);
        }
    }
}
