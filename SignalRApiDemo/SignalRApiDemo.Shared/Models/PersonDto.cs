﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignalRApiDemo.Shared.Models
{
    public class PersonDto
    {
        public PersonDto(int id, string name, string companyName)
        {
            Id = id;
            Name = name;
            CompanyName = companyName;
        }

        public int Id { get; }

        public string Name { get; }

        public string CompanyName { get; }
    }
}
