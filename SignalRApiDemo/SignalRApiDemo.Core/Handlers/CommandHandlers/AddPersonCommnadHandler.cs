﻿using FluentResults;
using FluentValidation;
using FluentValidation.Results;
using MediatR;
using SignalRApiDemo.Core.Handlers.Commands;
using SignalRApiDemo.Core.Services;
using SignalRApiDemo.Shared.Models;

namespace SignalRApiDemo.Core.Handlers.CommandHandlers
{
    internal class AddPersonCommnadHandler(IPersonSignalRService _service, IValidator<PersonDto> _validator) : IRequestHandler<AddPersonCommnad, Result>
    {
        public async Task<Result> Handle(AddPersonCommnad request, CancellationToken cancellationToken)
        {
            ValidationResult result = await _validator.ValidateAsync(request.PersonDto, cancellationToken);

            if (!result.IsValid)
            {
                return Result.Fail(result.Errors.Select(e => e.ErrorMessage));
            }

            await _service.SendNewPersonToClientsAsync(request.PersonDto, cancellationToken);

            return Result.Ok();
        }
    }
}
