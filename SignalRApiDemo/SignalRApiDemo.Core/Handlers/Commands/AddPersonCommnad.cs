﻿using FluentResults;
using MediatR;
using SignalRApiDemo.Shared.Models;

namespace SignalRApiDemo.Core.Handlers.Commands
{
    public class AddPersonCommnad(PersonDto _person) : IRequest<Result>
    {
        public PersonDto PersonDto => _person;
    }
}
