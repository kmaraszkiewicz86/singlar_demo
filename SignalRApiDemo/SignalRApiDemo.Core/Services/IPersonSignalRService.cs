﻿using SignalRApiDemo.Shared.Models;

namespace SignalRApiDemo.Core.Services
{
    public interface IPersonSignalRService
    {
        Task SendNewPersonToClientsAsync(PersonDto person, CancellationToken cancellationToken);
    }
}
