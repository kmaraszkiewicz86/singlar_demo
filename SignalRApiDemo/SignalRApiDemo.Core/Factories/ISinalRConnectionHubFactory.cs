﻿using Microsoft.AspNetCore.SignalR.Client;

namespace SignalRApiDemo.Core.Factories
{
    public interface ISinalRConnectionHubFactory
    {
        HubConnection PersonHubConnection { get; }
    }
}
