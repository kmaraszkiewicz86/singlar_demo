﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using SignalRApiDemo.Shared.Models;

namespace SignalRApiDemo.Core.Validators
{
    public class PersonValidator : AbstractValidator<PersonDto>
    {
        public PersonValidator()
        {
            RuleFor(p => p.Name)
                .NotEmpty()
                .MaximumLength(100);

            RuleFor(p => p.CompanyName)
                .NotEmpty()
                .MaximumLength(80);
        }
    }
}
