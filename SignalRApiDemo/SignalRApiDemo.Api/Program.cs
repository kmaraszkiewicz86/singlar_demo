using FluentResults;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using SignalRApiDemo.Configuration;
using SignalRApiDemo.Core.Handlers.Commands;
using SignalRApiDemo.Shared.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.RegisterCoreComponents()
    .RegisterSignalrAdapterComponents("http://localhost:5103");

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.MapSignalrComponents();
app.UseHttpsRedirection();

app.MapPost("/person", async (IMediator mediator, [FromBody] PersonDto person, CancellationToken cancellationToken) =>
{
    Result result = await mediator.Send(new AddPersonCommnad(person));

    return result;
})
.WithName("AddPerson")
.WithOpenApi();

app.Run();

internal record WeatherForecast(DateOnly Date, int TemperatureC, string? Summary)
{
    public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);
}
