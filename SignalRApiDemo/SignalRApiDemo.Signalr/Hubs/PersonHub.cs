﻿using Microsoft.AspNetCore.SignalR;
using SignalRApiDemo.Shared.Commons;
using SignalRApiDemo.Shared.Models;

namespace SignalRApiDemo.Signalr.Hubs
{
    public class PersonHub : Hub
    {
        public async Task SendPersonToAllClientsAsync(PersonDto person, CancellationToken cancellationToken)
        {
            await Clients.All.SendAsync(HubMethodNames.ReceiveNewPersonMethodName, person, cancellationToken);
        }
    }
}
