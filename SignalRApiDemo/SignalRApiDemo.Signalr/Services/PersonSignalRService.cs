﻿using Microsoft.AspNetCore.SignalR.Client;
using SignalRApiDemo.Core.Factories;
using SignalRApiDemo.Core.Services;
using SignalRApiDemo.Shared.Commons;
using SignalRApiDemo.Shared.Models;

namespace SignalRApiDemo.Signalr.Services
{
    public class PersonSignalRService(ISinalRConnectionHubFactory _hubConnectionFactory) : IPersonSignalRService
    {
        public async Task SendNewPersonToClientsAsync(PersonDto person, CancellationToken cancellationToken)
        {
            await _hubConnectionFactory
                .PersonHubConnection
                .SendAsync(HubEndpointsUrls.PersonHub, person, cancellationToken);
        }
    }
}
