﻿using Microsoft.AspNetCore.SignalR.Client;
using SignalRApiDemo.Core.Factories;
using SignalRApiDemo.Shared.Commons;

namespace SignalRApiDemo.Signalr.Factories
{
    public class SinalRConnectionHubFactory(string _apiUrl) : ISinalRConnectionHubFactory
    {
        public HubConnection PersonHubConnection => new HubConnectionBuilder()
            .WithUrl($"{_apiUrl}/{HubEndpointsUrls.PersonHub}")
            .WithAutomaticReconnect()
            .Build();
    }
}
