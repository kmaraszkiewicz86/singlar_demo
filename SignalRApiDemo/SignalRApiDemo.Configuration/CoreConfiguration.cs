﻿using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using SignalRApiDemo.Core.Handlers.Commands;
using SignalRApiDemo.Core.Validators;

namespace SignalRApiDemo.Configuration
{
    public static class CoreConfiguration
    {
        public static IServiceCollection RegisterCoreComponents(this IServiceCollection services)
        {
            services.AddValidatorsFromAssembly(typeof(PersonValidator).Assembly);
            services.AddMediatR(config =>
            {
                config.RegisterServicesFromAssembly(typeof(AddPersonCommnad).Assembly);
            });

            return services;
        }
    }
}
