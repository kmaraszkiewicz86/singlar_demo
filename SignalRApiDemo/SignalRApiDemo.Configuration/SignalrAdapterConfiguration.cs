﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using SignalRApiDemo.Core.Factories;
using SignalRApiDemo.Core.Services;
using SignalRApiDemo.Shared.Commons;
using SignalRApiDemo.Signalr.Factories;
using SignalRApiDemo.Signalr.Hubs;
using SignalRApiDemo.Signalr.Services;

namespace SignalRApiDemo.Configuration
{
    public static class SignalrAdapterConfiguration
    {
        public static IServiceCollection RegisterSignalrAdapterComponents(this IServiceCollection services, string apiUrl)
        {
            services.AddSignalR();
            services.AddScoped<ISinalRConnectionHubFactory>(s => new SinalRConnectionHubFactory(apiUrl));
            services.AddScoped<IPersonSignalRService, PersonSignalRService>();

            return services;
        }

        public static void MapSignalrComponents(this WebApplication app)
        {
            app.MapHub<PersonHub>(HubEndpointsUrls.PersonHub);
        }
    }
}
